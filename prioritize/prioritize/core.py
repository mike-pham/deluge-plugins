#
# core.py
#
# Copyright (C) 2009 Mike Pham <mpham@nativecode.com>
#
# Basic plugin template created by:
# Copyright (C) 2008 Martijn Voncken <mvoncken@gmail.com>
# Copyright (C) 2007-2009 Andrew Resch <andrewresch@gmail.com>
# Copyright (C) 2009 Damien Churchill <damoxc@gmail.com>
#
# Deluge is free software.
#
# You may redistribute it and/or modify it under the terms of the
# GNU General Public License, as published by the Free Software
# Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# deluge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with deluge.    If not, write to:
#   The Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor
#   Boston, MA  02110-1301, USA.
#
#    In addition, as a special exception, the copyright holders give
#    permission to link the code of portions of this program with the OpenSSL
#    library.
#    You must obey the GNU General Public License in all respects for all of
#    the code used other than OpenSSL. If you modify file(s) with this
#    exception, you may extend this exception to your version of the file(s),
#    but you are not obligated to do so. If you do not wish to do so, delete
#    this exception statement from your version. If you delete this exception
#    statement from all source files in the program, then also delete it here.
#

from deluge.log import LOG as log
from deluge.plugins.pluginbase import CorePluginBase
import deluge.component as component
import deluge.configmanager
from deluge.core.rpcserver import export

DEFAULT_PREFS = {
    "enable_trackers": True,
    "enable_max_requeue" : True,
    "max_requeue_count": 10,
    "max_zombie_timeout": 300,
    "priority_trackers": []
}

class Core(CorePluginBase):
    def enable(self):
        self.config = deluge.configmanager.ConfigManager("prioritize.conf", DEFAULT_PREFS)
        self.config.save()

        self.torrents = deluge.configmanager.ConfigManager("prioritize_states.conf", {})
        self.torrents.save()

        eventmanager = component.get("EventManager")
        eventmanager.register_event_handler("TorrentAddedEvent", self.handle_added)
        eventmanager.register_event_handler("TorrentStateChangedEvent", self.handle_state_changed)

    def disable(self):
        eventmanager = component.get("EventManager")
        eventmanager.deregister_event_handler("TorrentStateChangedEvent", self.handle_state_changed)
        eventmanager.deregister_event_handler("TorrentAddedEvent", self.handle_added)

    def update(self):
        try:
            self.update_queue_position()
        except Exception, e:
            log.exception(e)

    @export
    def set_config(self, config):
        """Sets the config dictionary"""
        for key in config.keys():
            self.config[key] = config[key]
        self.config.save()

    @export
    def get_config(self):
        """Returns the config dictionary"""
        return self.config.config

    @export
    def get_torrents(self):
        return self.torrents.config

    @export
    def add_tracker(self, tracker):
        if not tracker in self.config["priority_trackers"]:
            self.config["priority_trackers"].append(tracker)
            self.config.save()

    @export
    def get_trackers(self):
        return self.config["priority_trackers"]

    @export
    def remove_tracker(self, tracker):
        if tracker in self.config["priority_trackers"]:
            self.config["priority_trackers"].remove(tracker)
            self.config.save()

    @export
    def set_exempt(self, torrent_ids, exempted):
        for torrent_id in torrent_ids:
            if torrent_id in self.torrents:
                self.torrents[torrent_id]["exempt"] = exempted

                if exempted:
                    torrentmanager = component.get("TorrentManager")
                    torrent = torrentmanager.torrents.get(torrent_id, None)
                    log.warning("Prioritize: Marked torrent '%s' as exempted.", torrent.filename)

        self.torrents.save()

    @export
    def get_requeue_count(self, torrent_id):
        return self.torrents[torrent_id] if torrent_id in self.torrents else -1

    def handle_added(self, torrent_id):
        try:
            torrentmanager = component.get("TorrentManager")
            torrent = torrentmanager.torrents.get(torrent_id, None)

            if self.can_prioritize_tracker(torrent, torrent.state):
                torrentmanager.queue_top(torrent_id)
                log.warning("Prioritize: Moving torrent '%s' to the top of the queue.", torrent.filename)
        except Exception, e:
            log.exception(e)

    def handle_state_changed(self, torrent_id, state):
        try:
            torrentmanager = component.get("TorrentManager")
            torrent = torrentmanager.torrents.get(torrent_id, None)

            if self.can_prioritize_tracker(torrent, state):
                torrentmanager.queue_top(torrent_id)
                log.warning("Prioritize: Moving torrent '%s' to the top of the queue.", torrent.filename)
        except Exception, e:
            log.exception(e)

    def can_prioritize_tracker(self, torrent, state):
        try:
            priority_trackers = self.config["priority_trackers"]
            hostname = torrent.get_tracker_host()

            # If the torrent is checking, downloading, or queued we want to allow prioritization by tracker.
            if state in ["Checking", "Downloading", "Queued"] and hostname in priority_trackers:
                log.warning("Prioritize: Torrent '%s' matched priority tracker '%s'.", torrent.filename, hostname)
                return True
        except Exception, e:
            log.exception(e)
            
        return False

    def update_queue_position(self):
        torrentmanager = component.get("TorrentManager")
        torrent_ids = torrentmanager.get_torrent_list()

        enable_max_requeue = self.config["enable_max_requeue"]
        max_requeue_count = self.config["max_requeue_count"]
        max_zombie_timeout = self.config["max_zombie_timeout"]

        for torrent_id in torrent_ids:
            torrent = torrentmanager.torrents.get(torrent_id, None)
            filename = torrent.filename

            # We want to ignore torrents that are queued, seeding, or paused.
            if torrent.state in ["Paused", "Queued", "Seeding"]:
                continue

            if not torrent_id in self.torrents:
                self.torrents[torrent_id] = { "exempt": False, "requeue_count": 0, "timer": 0 }

            current = self.torrents[torrent_id]
            seeder_count = torrent.status.num_seeds

            # If the torrent is exempted, we skip it.
            if current["exempt"]:
                current["requeue_count"] = 0
                current["timer"] = 0
                continue

            # Update the torrent's timer depending on the count of seeders.
            if seeder_count == 0:
                current["timer"] += 1

                if current["timer"] % 60 == 0:
                    log.warning("Prioritize: Torrent '%s' seems like it no longer has seeders.", filename)
            else:
                # Reset requeue counter if the torrent started downloading again.
                if current["timer"] > 0:
                    log.warning("Prioritize: Torrent '%s' seems to be downloading again.", filename)
                    current["requeue_count"] = 0

                current["timer"] = 0

            # Move to the bottom of the queue and increment the requeue counter.
            if current["timer"] > max_zombie_timeout and seeder_count == 0:
                torrentmanager.queue_bottom(torrent_id)
                current["requeue_count"] += 1
                current["timer"] = 0
                log.warning("Prioritize: Moving torrent '%s' to the bottom of the queue, requeued %s times.", filename, current["requeue_count"])

            # Pause the torrent if we reached the max requeue counter.
            if enable_max_requeue and current["requeue_count"] == max_requeue_count:
                torrent.pause()
                current["requeue_count"] = 0
                current["timer"] = 0
                log.warning("Prioritize: Pausing torrent '%s' due to max requeue count %s.", filename, max_requeue_count)

        self.torrents.save()
