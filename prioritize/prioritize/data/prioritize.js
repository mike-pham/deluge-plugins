/*
Script: prioritize.js
    The client-side javascript code for the Prioritize plugin.

Copyright:
    (C) Mike Pham 2009 <mpham@nativecode.com>
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, write to:
        The Free Software Foundation, Inc.,
        51 Franklin Street, Fifth Floor
        Boston, MA  02110-1301, USA.

    In addition, as a special exception, the copyright holders give
    permission to link the code of portions of this program with the OpenSSL
    library.
    You must obey the GNU General Public License in all respects for all of
    the code used other than OpenSSL. If you modify file(s) with this
    exception, you may extend this exception to your version of the file(s),
    but you are not obligated to do so. If you do not wish to do so, delete
    this exception statement from your version. If you delete this exception
    statement from all source files in the program, then also delete it here.
*/

Ext.namespace('Deluge.plugins.prioritize.ui');

Deluge.plugins.prioritize.DISPLAY_NAME = _('Prioritize');
Deluge.plugins.prioritize.PLUGIN_NAME = 'prioritize';

Deluge.plugins.prioritize.Plugin = Ext.extend(Deluge.Plugin, {
    name: Deluge.plugins.prioritize.PLUGIN_NAME,

    onDisable: function() {
        deluge.preferences.removePage(this.preferences);
    },

    onEnable: function() {
        this.preferences = deluge.preferences.addPage(new Deluge.plugins.prioritize.ui.PreferencePage());
    }
});

Deluge.plugins.prioritize.ui.PreferencePage = Ext.extend(Ext.Panel, {

    title: Deluge.plugins.prioritize.DISPLAY_NAME,
    border: false,
    layout: 'vbox',

    initComponent: function() {
        Deluge.plugins.prioritize.ui.PreferencePage.superclass.initComponent.call(this);

        this.list = new Ext.list.ListView({
            store: new Ext.data.ArrayStore({
                fields: ['tracker'],
                id: 0
            }),
            columns: [{
                header: _('Host Name'),
                sortable: true,
                dataIndex: 'tracker'
            }],
            singleSelect: true,
            autoExpandColumn: 'name'
        });
        this.list.on('selectionchange', this.onSelectionChange, this);

        this.settings = this.add({
            title: 'General Settings',
            height: 68,
            items: [{
                border: false,
                margins: '5 5 5 5',
                items: [{
                    xtype: 'checkbox',
                    boxLabel: 'Enable Max Requeue',
                    handler: this.onEnableMaxRequeueClick
                }, {
                    xtype: 'checkbox',
                    boxLabel: 'test'
                }]
            }]
        });

        this.panel = this.add({
            title: 'Priority Trackers',
            items: [this.list],
            tbar: {
                items: [{
                    xtype: 'checkbox',
                    boxLabel: 'Enable Priority Trackers',
                    margins: '5 5 5 5',
                    handler: this.onEnableTrackersClick,
                    scope: this
                }]
            },
            bbar: {
                items: [{
                    text: _('Add'),
                    iconCls: 'icon-add',
                    handler: this.onAddClick,
                    disabled: false,
                    scope: this
                }, {
                    text: _('Edit'),
                    iconCls: 'icon-edit',
                    handler: this.onEditClick,
                    scope: this,
                    disabled: true
                }, '->', {
                    text: _('Remove'),
                    iconCls: 'icon-remove',
                    handler: this.onRemoveClick,
                    scope: this,
                    disabled: true
                }]
            }
        });

        deluge.preferences.on('show', this.onPreferencesShow, this);
    },

    updateTrackerCheckboxState: function(enable) {
        if (enable) {
            this.list.enable();
            this.panel.getBottomToolbar().enable();
        } else {
            this.list.disable();
            this.panel.getBottomToolbar().disable();
        }
    },

    updateSettings: function() {
        deluge.client.prioritize.get_config({
            success: function(config) {
                this.settings.items.get(0).items.get(0).checked = config.enable_max_requeue;

                this.panel.getTopToolbar().items.get(0).checked = config.enable_trackers;
                this.updateTrackerCheckboxState(config.enable_trackers);
            },
            scope: this
        });
    },

    updateTrackers: function() {
        deluge.client.prioritize.get_trackers({
            success: function(trackers) {
                var data = [];
                for (var index = 0; index < trackers.length; index++) {
                    data.push([trackers[index]]);
                }
                this.list.store.loadData(data);
            },
            scope: this
        })
    },

    onPreferencesShow: function(item, e) {
        this.updateSettings();
        this.updateTrackers();
    },

    onAddClick: function(item, e) {
        if (!this.editor) {
            this.editor = new Deluge.plugins.prioritize.ui.AddTrackerWindow();
            this.editor.on('command_add_tracker', function() {
                this.updateTrackers();
            }, this);
        }

        this.editor.show();
    },

    onEditClick: function(item, e) {
        if (!this.editor) {
            this.editor = new Deluge.plugins.prioritize.ui.EditTrackerWindow;
            this.editor.on('command_edit_tracker', function() {
                this.updateTrackers();
            }, this);
        }

        this.editor.show(this.trackers.getSelectedRecords()[0]);
    },

    onRemoveClick: function(item, e) {
        var record = this.trackers.getSelectedRecords()[0];
        deluge.client.prioritize.remove_tracker(record.id, {
            success: function() {
                this.updateTrackers();
            },
            scope: this
        });
    },

    onSelectionChange: function(item, selections) {
        if (selections.length) {
            this.panel.getBottomToolbar().items.get(1).enable();
            this.panel.getBottomToolbar().items.get(3).enable();
        } else {
            this.panel.getBottomToolbar().items.get(1).disable();
            this.panel.getBottomToolbar().items.get(3).disable();
        }
    },

    onEnableTrackersClick: function(item, e) {
        this.updateTrackerCheckboxState(item.checked);
        deluge.client.prioritize.set_config({ enable_trackers: item.checked });
    },

    onEnableMaxRequeueClick: function(item, e) {
        deluge.client.prioritize.set_config({ enable_max_requeue: item.checked });
    }

});

Deluge.plugins.prioritize.ui.PrioritizeWindowBase = Ext.extend(Ext.Window, {

    layout: 'fit',
    width: 300,
    height: 100,
    closeAction: 'hide',
    modal: true,

    initComponent: function() {
        Deluge.plugins.prioritize.ui.PrioritizeWindowBase.superclass.initComponent.call(this);
        this.addButton(_('Cancel'), this.onCancelClick, this);

        this.form = this.add({
            xtype: 'form',
            baseCls: 'x-plain',
            bodyStyle: 'padding: 5px',
            items: [{
                xtype: 'textfield',
                fieldLabel: _('Tracker'),
                name: 'tracker',
                width: 170
            }]
        });
    },

    onCancelClick: function() {
        this.hide();
    }

});

Deluge.plugins.prioritize.ui.AddTrackerWindow = Ext.extend(Deluge.plugins.prioritize.ui.PrioritizeWindowBase, {

    title: _('Add Tracker'),

    initComponent: function() {
        Deluge.plugins.prioritize.ui.AddTrackerWindow.superclass.initComponent.call(this);
        this.addButton(_('Add'), this.onAddClick, this);
        this.addEvents({'command_add_tracker': true});
    },

    onAddClick: function() {
        var values = this.form.getForm().getFieldValues();

        deluge.client.prioritize.add_tracker(values.tracker, {
            success: function() {
                this.fireEvent('command_add_tracker', this, values.tracker);
            },
            scope: this
        });

        this.hide();
    }

});

Deluge.plugins.prioritize.ui.EditTrackerWindow = Ext.extend(Deluge.plugins.prioritize.ui.PrioritizeWindowBase, {

    title: _('Edit Tracker'),

    initComponent: function() {
        Deluge.plugins.prioritize.ui.EditTrackerWindow.superclass.initComponent.call(this);
        this.addButton(_('Save'), this.onSaveClick, this);
        this.addEvents({'command_edit_tracker': true});
    },

    show: function(record) {
        Deluge.plugins.prioritize.ui.EditTrackerWindow.superclass.show.call(this);
        this.record = record;

        this.form.getForm().setValues({ tracker: record.get('tracker') });
    },

    onSaveClick: function() {
        var values = this.form.getForm().getFieldValues();

        // TODO: Hack until there's a real exported rename_tracker method.
        deluge.client.prioritize.remove_tracker(this.record.get('tracker'), {
            success: function() {
                deluge.client.prioritize.add_tracker(values.tracker, {
                    success: function() {
                        this.fireEvent('command_edit_tracker', this, values.tracker);
                    },
                    scope: this
                });
            },
            scope: this
        });

        this.hide();
    }

});

Deluge.registerPlugin(Deluge.plugins.prioritize.PLUGIN_NAME, Deluge.plugins.prioritize.Plugin);
