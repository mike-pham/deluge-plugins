#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
CWD=$(pwd)

cd $DIR
mkdir temp
export PYTHONPATH=./temp

/usr/bin/python setup.py build develop --install-dir ./temp

cp ./temp/SmartRemove.egg-link ~/.config/deluge/plugins/
rm -fr ./temp

cd $CWD
