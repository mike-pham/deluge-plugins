#
# core.py
#
# Copyright (C) 2009 Mike Pham <mpham@nativecode.com>
#
# Basic plugin template created by:
# Copyright (C) 2008 Martijn Voncken <mvoncken@gmail.com>
# Copyright (C) 2007-2009 Andrew Resch <andrewresch@gmail.com>
# Copyright (C) 2009 Damien Churchill <damoxc@gmail.com>
#
# Deluge is free software.
#
# You may redistribute it and/or modify it under the terms of the
# GNU General Public License, as published by the Free Software
# Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# deluge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with deluge.    If not, write to:
# 	The Free Software Foundation, Inc.,
# 	51 Franklin Street, Fifth Floor
# 	Boston, MA  02110-1301, USA.
#
#    In addition, as a special exception, the copyright holders give
#    permission to link the code of portions of this program with the OpenSSL
#    library.
#    You must obey the GNU General Public License in all respects for all of
#    the code used other than OpenSSL. If you modify file(s) with this
#    exception, you may extend this exception to your version of the file(s),
#    but you are not obligated to do so. If you do not wish to do so, delete
#    this exception statement from your version. If you delete this exception
#    statement from all source files in the program, then also delete it here.
#

import deluge.component as component
import deluge.configmanager
import os.path

from deluge.log import LOG as log
from deluge.plugins.pluginbase import CorePluginBase
from deluge.core.rpcserver import export

DEFAULT_PREFS = {
    "allow_remove_data": False,
    "allow_remove_ignored_files": True,
    "enable_tracker_ratios": False,
    "tracker_ratios": {}
}

class Core(CorePluginBase):
    def enable(self):
        self.config = deluge.configmanager.ConfigManager("smartremove.conf", DEFAULT_PREFS)
        self.config.save()

        eventmanager = component.get("EventManager")
        eventmanager.register_event_handler("PreTorrentRemovedEvent", self.handle_pre_removed)
        eventmanager.register_event_handler("TorrentStateChangedEvent", self.handle_state_changed)
        eventmanager.register_event_handler("TorrentFinishedEvent", self.handle_finished)

    def disable(self):
        eventmanager = component.get("EventManager")
        eventmanager.deregister_event_handler("TorrentFinishedEvent", self.handle_finished)
        eventmanager.deregister_event_handler("TorrentStateChangedEvent", self.handle_state_changed)
        eventmanager.deregister_event_handler("PreTorrentRemovedEvent", self.handle_pre_removed)

    def update(self):
        pass

    @export
    def set_config(self, config):
        """Sets the config dictionary"""
        for key in config.keys():
            self.config[key] = config[key]
        self.config.save()

    @export
    def get_config(self):
        """Returns the config dictionary"""
        return self.config.config

    def get_file_indices(self, torrent):
        indices = []

        for index, priority in enumerate(torrent.options["file_priorities"]):
            if priority == 0:
                indices.append(index)

        return indices

    def handle_finished(self, torrent_id):
        log.debug("SmartRemove: handle_finished torrent_id: %s", torrent_id)
        self.remove_torrent(torrent_id)

    def handle_pre_removed(self, torrent_id):
        log.debug("SmartRemove: handle_pre_removed torrent_id: %s", torrent_id)
        try:
            if self.config["allow_remove_ignored_files"]:
                torrentmanager = component.get("TorrentManager")
                torrent = torrentmanager.torrents.get(torrent_id, None)
                self.remove_ignored_files(torrent)
        except Exception, e:
            log.exception(e)

    def handle_state_changed(self, torrent_id, state):
        log.debug("SmartRemove: handle_state_changed torrent_id: %s, state: %s", torrent_id, state)
        self.remove_torrent(torrent_id)

    def can_remove_torrent(self, torrent):
        enable_tracker_ratios = self.config["enable_tracker_ratios"]

        if enable_tracker_ratios:
            tracker_ratios = self.config["tracker_ratios"]
            hostname = torrent.get_tracker_host()

            log.debug("SmartRemove: can_remove_torrent '%s' (%s), state: %s, ratio: %s",
                torrent.filename, torrent.torrent_id, torrent.state, ratio)

            if hostname in tracker_ratios and torrent.options["ratio"] >= self.tracker_ratios[hostname]:
                return True

        log.debug("SmartRemove: can_remove_torrent '%s' (%s), state: %s, is_finished: %s",
            torrent.filename, torrent.torrent_id, torrent.state, str(torrent.is_finished))

        return torrent.state in ["Queued", "Seeding"] and torrent.is_finished

    def remove_torrent(self, torrent_id):
        try:
            allow_remove_data = self.config["allow_remove_data"]
            torrentmanager = component.get("TorrentManager")
            torrent = torrentmanager.torrents.get(torrent_id, None)

            if torrent is not None and self.can_remove_torrent(torrent):
                log.debug("SmartRemove: handle_state_changed '%s' (%s)", torrent.filename, torrent_id)

                if torrentmanager.remove(torrent_id, allow_remove_data):
                    log.warning("SmartRemove: Removing completed torrent '%s'.", torrent.filename)
                    self.remove_ignored_files(torrent)
        except Exception, e:
            log.exception(e)

    def remove_ignored_files(self, torrent):
        if torrent is None:
            return

        removable_directories = []

        files = torrent.get_files()
        location = torrent.options["download_location"]
        priorities = torrent.options["file_priorities"]

        for file_item, priority in zip(files, priorities):
            # Skip files that were not marked as Do Not Download.
            if priority != 0:
                continue

            path = file_item["path"]

            # Add to the list of empty directories we need to clean up.
            if os.path.dirname(path) != "":
                removable_directories.append(os.path.dirname(path))

            # Get the full path and remove the file.
            filepath = os.path.join(location, path)

            try:
                if os.path.isfile(filepath):
                    os.remove(path)
                    log.warning("SmartRemove: Delete file '%s'.", filepath)
            except Exception, e:
                log.exception(e)
                continue

        for directory in set(removable_directories):
            directory_path = os.path.join(location, directory)

            try:
                if len(os.listdir(directory_path) == 0):
                    os.removedirs(directory_path)
                    log.warning("SmartRemove: Deleted directory '%s'.", directory_path)
            except Exception, e:
                log.exception(e)
                continue
