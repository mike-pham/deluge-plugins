#!/bin/bash

python ../deluge/deluge/scripts/create_plugin.py --name $1 --basepath . \
	--author-name "Mike Pham" --author-email "mpham@nativecode.com"
