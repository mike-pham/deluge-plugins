#!/bin/bash

#python ./prioritize/setup.py bdist_egg
#python ./smartremove/setup.py bdist_egg

for file in *; do
	if [ -d "$file" ]; then
		# Compile
		cd $file
		python setup.py bdist_egg

		# Copy
		cp dist/*.egg ../../releases/

		# Cleanup
		rm -rf build
		rm -rf dist
		rm -rf *.egg-info

		# Exit
		cd ..
	fi
done
